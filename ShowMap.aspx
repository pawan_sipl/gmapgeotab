<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="ShowMap.aspx.cs" Inherits="ShowMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContainer" Runat="Server">
    <div class="row">
        <div class="container">
            <div class="clearfix"></div>
            <br />
            <div class="page-header">
                <h1><small>Proof of concept</small></h1>
            </div>
        </div>
        <div class="col-sm-2 col-md-2">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus">&nbsp;</span>Filters</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h5><a id="btnShowCrew" style="cursor:pointer">Crews</a></h5>
                                <ul class="list-group">
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-primary">&nbsp;</span><a title="Coming Soon">Status</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-success">&nbsp;</span><a title="Coming Soon">Skill</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning">&nbsp;</span><a title="Coming Soon">Shifts</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning">&nbsp;</span><a title="Coming Soon">Size</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning">&nbsp;</span><a title="Coming Soon">Availability</a></li>
                                </ul>
                            </li>
                            <li class="list-group-item">
                                <h5> <a style="cursor:pointer" id="btnVehicleLocation">Vehicle Location</a></h5>
                                <%--  <h5><button class="btn btn-danger btn-sm btnInShop"  id="btnInShop" style="width:105px; height:35px;">In Shop</button></h5>
                                  <h5><button class="btn btn-danger btn-sm btnInActive"  id="btnInActive" style="width:105px; height:35px;">In Active</button></h5>
                                  <h5><button class="btn btn-danger btn-sm btnActive" id="btnActive" style="width:105px; height:35px;">Active</button></h5>--%>
                                
                                <ul class="list-group">
                                  
                                    <li class="list-group-item "><span class="glyphicon glyphicon-minus text-success">&nbsp;</span><a id="aInShop" style="cursor:pointer">In Shop</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning  ">&nbsp;</span><a id="aInActive" style="cursor:pointer">Inactive</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning ">&nbsp;</span><a id="aActive" style="cursor:pointer">Active</a></li>
                                     
                                   
                                </ul>
                            </li>
                            <li class="list-group-item">
                                <h5> <a style="cursor:pointer" id="btnVehicleType">Vehicle Type</a></h5>
                                <%--  <h5><button class="btn btn-danger btn-sm btnInShop"  id="btnInShop" style="width:105px; height:35px;">In Shop</button></h5>
                                  <h5><button class="btn btn-danger btn-sm btnInActive"  id="btnInActive" style="width:105px; height:35px;">In Active</button></h5>
                                  <h5><button class="btn btn-danger btn-sm btnActive" id="btnActive" style="width:105px; height:35px;">Active</button></h5>--%>
                                
                                <ul class="list-group">                                  
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-success">&nbsp;</span><a id="aVan" style="cursor:pointer">Van</a>
                                        <div><span style="font-size:medium;color:red;font-weight:bold">- </span><a id="a7Passenger" style="cursor:pointer">7 Passenger</a></div>
                                        <div><span style="font-size:medium;color:red;font-weight:bold">- </span><a id="a15Passenger" style="cursor:pointer">15 Passenger</a></div>                                       
                                    </li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning  ">&nbsp;</span><a id="aTruck" style="cursor:pointer">Truck</a>
                                        <div><span style="font-size:medium;color:red;font-weight:bold">- </span><a id="aPickUp" style="cursor:pointer">Pick up</a></div>
                                        <div><span style="font-size:medium;color:red;font-weight:bold">- </span><a id="aUtilityLight" style="cursor:pointer">Utility light</a></div>
                                    </li>                                                                      
                                </ul>
                            </li>
                            <li class="list-group-item">
                                <h5>WO</h5>
                                <ul class="list-group">
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-success">&nbsp;</span><a title="Coming Soon">Status</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning">&nbsp;</span><a title="Coming Soon">Time</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning">&nbsp;</span><a title="Coming Soon">Reason</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-primary">&nbsp;</span><a title="Coming Soon">Priority</a></li>
                                </ul>
                            </li>
                            <li class="list-group-item">
                                <h5>Zones</h5>
                                <ul class="list-group">
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-warning">&nbsp;</span><a title="Coming Soon">Location</a></li>
                                    <li class="list-group-item"><span class="glyphicon glyphicon-minus text-success">&nbsp;</span><a title="Coming Soon">Type</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Geo Information &nbsp;: <span id="spnLabel" style="font-size:medium;font-weight:bold;"></span></h3>
                </div>
                <div class="panel-body">
                    <div class="row padbig">
                        <div id="map" class="map">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2 col-md-2">
            <div class="panel-heading"style="padding-top:0px;" >
                <ul class="list-group">
                     <li class="list-group-item">
                        <a id="btnShowVehicles" class="btn btn-danger btn-sm btnShowVehicles" style="width:105px; height:35px;">Show All Vehicle</a>
                    </li>
                      <li class="list-group-item">
                        <a id="btnDropMarker" class="btn btn-danger btn-sm" style="width:105px; height:35px;">Drop Marker</a>
                    </li>
                    <li class="list-group-item">
                        <a id="btnHydrant" class="btn btn-danger btn-sm" style="width:105px; height:35px;">Hydrants</a>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Statistic</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Raise WO</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Nearest Crew</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Email Crew</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Geotab</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Dispatch</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Alert All</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Mobilize Unit</button>
                    </li>
                    <li class="list-group-item" title="Coming Soon">
                        <button class="btn btn-danger btn-sm" style="width:105px; height:35px;" disabled="disabled">Change Status</button>
                    </li>
                </ul>
            </div>          
        </div>
    </div>
    <!--- Google Maps Api Js--->
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
     <style type="text/css">
        .info_content {
            background-color:antiquewhite;       
             width:200px; 
             padding : 10px 10px 10px 10px;    
             border: #000 1px solid;      
        }
         #active {
             color:#51bf05;
             font-size:medium;
         }
         #inactive {
          color:#f00;
          font-size:medium;
         
          }
    </style>
     <!--- Load Google Map--->
    <script type="text/javascript">
        var map;
        // Info Window Content
        var infoWindowContent = [
            ['<div class="info_content active">' +
            '<p><strong>Vehicle Type :</strong> VAN</p>' +
            '<p><strong>Vehicle Status : </strong> <span id="active">ACTIVE</span></p>' +
            '<p><strong>Equipped As :</strong> 7 PASSENGER </p>' +
            '<p><strong>Priority :</strong> 1 </p>' +
            '</div>'],
            ['<div class="info_content active">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong> <span id="active">ACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> PICK UP </p>' +
            '<p><strong>Priority :</strong> 1 </p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong><span id="active">ACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> UTILITY LIGHT </p>' +
            '<p><strong>Priority :</strong> 1 </p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</h5>' +
            '<p><strong>Vehicle Status : </strong><span id="active">ACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> UTILITY LIGHT </p>' +
            '<p><strong>Priority :</strong> 1 </p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>Vehicle Type :</strong>VAN</p>' +
            '<p><strong>Vehicle Status : </strong><span id="active">IN SHOP</span> </p>' +
            '<p><strong>Equipped As :</strong> 15 PASSENGER </p>' +
            '<p><strong>Priority :</strong> 2 </p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong><span id="active">ACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> UTILITY LIGHT </p>' +
            '<p><strong>Priority :</strong> 3 </p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong><span id="active">ACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> UTILITY LIGHT </p>' +
            '<p><strong>Priority :</strong> 3 </p>' +
            '</div>'],
             ['<div class="info_content inshop">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong> <span id="inactive">INACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> PICK UP </p>' +
            '<p><strong>Priority :</strong> 2 </p>' +
            '</div>'],
             ['<div class="info_content inshop">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong> <span id="active">IN SHOP</span>  </p>' +
            '<p><strong>Equipped As :</strong> PICK UP </p>' +
            '<p><strong>Priority :</strong> 2 </p>' +
            '</div>'],
             ['<div class="info_content inactive">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong><span id="active">ACTIVE</span>  </p>' +
            '<p><strong>Equipped As :</strong> UTILITY LIGHT </p>' +
            '<p><strong>Priority :</strong> 1 </p>' +
            '</div>'],
             ['<div class="info_content inactive">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong><span id="inactive">INACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> UTILITY LIGHT </p>' +
            '<p><strong>Priority :</strong> 2 </p>' +
            '</div>'],
             ['<div class="info_content inactive">' +
            '<p><strong>Vehicle Type :</strong> TRUCK</p>' +
            '<p><strong>Vehicle Status : </strong><span id="inactive">INACTIVE</span> </p>' +
            '<p><strong>Equipped As :</strong> UTILITY LIGHT </p>' +
            '<p><strong>Priority :</strong> 3 </p>' +
            '</div>'],
            
        ];


        // Info Window Content
        var infoHydrantContent = [
            ['<div class="info_content active">' +
            '<p><strong>H00184</strong></p>' +
            '<p><span>Hydrant, CLOW - EDDY</span></p>' +
            '<p>Not In Service</p>' +            
            '</div>'],
            ['<div class="info_content active">' +
            '<p><strong>H00513</strong></p>' +
            '<p><span>Hydrant, MUELLER-SUP CENT 250</span></p>' +
            '<p>Not In Service</p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>H00531</strong></p>' +
            '<p><span>Hydrant, MUELLER-SUP CENT 250</span></p>' +
            '<p>Not In Service</p>' +
            '</div>'],
             ['<div class="info_content active">' +
           '<p><strong>H00640</strong></p>' +
            '<p><span>Hydrant, LORTON - OBRIEN</span></p>' +
            '<p>Not In Service</p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>H00798</strong></p>' +
            '<p><span>Hydrant, KENNEDY - K10/K11/K12</span></p>' +
            '<p>Not In Service</p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>H00002</strong></p>' +
            '<p><span>Hydrant, KENNEDY-K81-A/D GUAR</span></p>' +
            '<p>In Service</p>' +
            '</div>'],
             ['<div class="info_content active">' +
            '<p><strong>H00007</strong></p>' +
            '<p><span>Hydrant, MUELLER-SUP CENT 250</span></p>' +
            '<p>In Service</p>' +
            '</div>'],
             ['<div class="info_content inshop">' +
            '<p><strong>H00011</strong></p>' +
            '<p><span>Hydrant, US PIPE-A P SMITH</span></p>' +
            '<p>In Service</p>' +
            '</div>'],           
             ['<div class="info_content inactive">' +
            '<p><strong>H00014</strong></p>' +
            '<p><span>Hydrant, KENNEDY-K81-A/D GUAR</span></p>' +
            '<p>In Service</p>' +
            '</div>'],           
             ['<div class="info_content inactive">' +
            '<p><strong>H00017</strong></p>' +
            '<p><span>Hydrant, MUELLER-SUP CENT 250</span></p>' +
            '<p>In Service</p>' +
            '</div>'],
        ];


       //Variables
        var locations = [

              ['VAN', 38.866574, -77.119904, 1],
              ['TRUCK', 38.8919, -76.941504, 2],
              ['TRUCK', 38.913, -77.041637, 3],
              ['TRUCK', 38.845999, -76.983325, 4],
              ['VAN', 38.850742, -76.983014, 5],
              ['TRUCK', 38.883721, -76.993443, 6],
              ['TRUCK', 38.885024, -77.005502, 7],
              ['TRUCK', 38.915548, -77.039142, 8],
              ['TRUCK', 38.844061, -76.99178, 9],
              ['TRUCK', 38.8448, -76.977822, 10],
              ['TRUCK', 38.849338, -76.978487, 11],
              ['TRUCK', 38.884272, -76.999451, 12]
        ];

        var crew = [
              ['Crew', 38.84574, -77.11504, 1],
              ['Crew', 38.89109, -76.941004, 2],
              ['Crew', 38.90233, -77.022637, 3],
              ['Crew', 38.832999, -76.973325, 4],
              ['Crew', 38.840742, -76.973014, 5],
              ['Crew', 38.882721, -76.992443, 6],
              ['Crew', 38.884024, -77.004502, 7]
        ];


        var hydrant = [
             ['Hydrant', 38.865459, -77.112704, 1],
             ['Hydrant', 38.84694, -76.98858, 2],
             ['Hydrant', 38.882701, -76.992994, 3],
             ['Hydrant', 38.841709, - 76.92676, 4],
             ['Hydrant', 38.915574, -77.03996, 5],
             ['Hydrant', 38.89270, -76.941755, 6],
             ['Hydrant', 38.851780, -77.091040, 7],
             ['Hydrant', 38.926609, -77.0397, 8],
             ['Hydrant', 38.80485, -77.0572, 9],
             ['Hydrant', 38.88495, -76.95107, 10]
        ];

            map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: new google.maps.LatLng(38.866574, -77.0094),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();
        var i;
        var gmarkers = [];
        var marker = new google.maps.Marker();

        // This function is to retrieve the In Shop Vehicles
        $("#aInShop").click(function () {
            $("#aActive").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#aInShop").css("font-weight", "Bold");
            $("#spnLabel").text("Vehicle Location (In Shop)");
            $("#btnVehicleLocation").css("font-weight", "Bold");
            $("#btnVehicleType").css("font-weight", "normal");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);               
            }
           //for 1st inshop
            for (i = 4; i < locations.length - 7; i++) {           
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);               
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {

                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 2nd inshop
            for (i = 8; i < locations.length - 3; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {

                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }           

            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');                
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {                    
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
           //return false;
        });

        // This function is to retrieve the Van Vehicles
        $("#aVan").click(function () {
            $("#aTruck").css("font-weight", "normal");
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#aVan").css("font-weight", "Bold");
            $("#spnLabel").text("Vehicle Type (Van)");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "Bold");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            //for 1st van
            for (i = 0; i < locations.length - 11; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 2nd van
            for (i = 4; i < locations.length - 7; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //return false;
        });


        // This function is to retrieve the 7 passenger Vehicles
        $("#a7Passenger").click(function () {
            $("#aTruck").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#aVan").css("font-weight", "Bold");
            $("#a7Passenger").css("font-weight", "Bold");
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#spnLabel").text("Vehicle Type (Van - 7 Passenger)");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "Bold");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            //for 1st van
            for (i = 0; i < locations.length - 11; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });
                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //return false;
        });


        // This function is to retrieve the 15 passenger Vehicles
        $("#a15Passenger").click(function () {
            $("#aTruck").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#aVan").css("font-weight", "Bold");
            $("#a15Passenger").css("font-weight", "Bold");
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#spnLabel").text("Vehicle Type (Van - 15 Passenger)");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "Bold");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            //for 1st van
            for (i = 4; i < locations.length - 7; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //return false;
        });


        // This function is to retrieve the Pickup Vehicles
        $("#aPickUp").click(function () {
            $("#aVan").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "Bold");
            $("#aPickUp").css("font-weight", "Bold");
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#spnLabel").text("Vehicle Type (Truck - Pick up)");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "Bold");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            //for 1st pick up
            for (i = 1; i < locations.length - 10; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 2 pick ups
            for (i = 7; i < locations.length - 3; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //return false;
        });


        // This function is to retrieve the Utility Lights Vehicles
        $("#aUtilityLight").click(function () {
            $("#aVan").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "Bold");
            $("#aUtilityLight").css("font-weight", "Bold");
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#spnLabel").text("Vehicle Type (Truck - Utility light)");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "Bold");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            //for 2 utility light
            for (i = 2; i < locations.length - 8; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 2 utility light
            for (i = 5; i < locations.length - 5; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 3 utility light
            for (i = 9; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //return false;
        });

        // This function is to retrieve the Truck Vehicles
        $("#aTruck").click(function () {
            $("#aVan").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "Bold");
            $("#aActive").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aInShop").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");            
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#spnLabel").text("Vehicle Type (Truck)");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "Bold");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            //for 1st truck
            for (i = 1; i < locations.length - 8; i++) {

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 2nd truck
            for (i = 5; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //return false;
        });


        // This function is to retrieve the In Active Vehicles
        $("#aInActive").click(function () {
            $("#spnLabel").text("Vehicle Location (Inactive)");
            $("#aActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "Bold");
            $("#aInShop").css("font-weight", "normal");
            $("#btnVehicleLocation").css("font-weight", "Bold");
            $("#btnVehicleType").css("font-weight", "normal");
            //Removing the list of markers from the array.
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
           //for 1st inactive
            for (i = 7; i < locations.length-4; i++) {            
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {

                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);

                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 2nd inactive
            for (i = 10; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {

                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);

                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
            }
            //return false;
        });


        // This function is to retrieve the Active Vehicles
        $("#aActive").click(function () {
            $("#spnLabel").text("Vehicle Location (Active)");
            $("#aActive").css("font-weight", "Bold");
            $("#aInActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#aInShop").css("font-weight", "normal");
            $("#btnVehicleLocation").css("font-weight", "Bold");
            $("#btnVehicleType").css("font-weight", "normal");
            //Removing the list of markers from the array. 
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
           //for 4 active
            for (i = 0; i < locations.length - 8; i++) {         
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);                
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 2 active
            for (i = 5; i < locations.length - 5; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //for 1 active
            for (i = 9; i < locations.length - 2; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            //Code to show hydrants
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }               
            }
            
           //return false;
        });
       

        var vflag = 0;
        //function to call ShowAllVehicle() .
        $("#btnShowVehicles").click(function () {
            $("#spnLabel").text("Show All Vehicle");
           // flag = flag + 1;
            ShowAllVehicle();
        });
               
        //function to call vehicle location() .
        $("#btnVehicleLocation").click(function () {
            $("#spnLabel").text("Vehicle Location");            
            $("#btnVehicleLocation").css('font-weight', 'bold');
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "normal");
            //Removing the list of markers from the array. 
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag =1;
            //hflag = 0;
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
            }
        });

        //function to call vehicle type() .
        $("#btnVehicleType").click(function () {
            $("#spnLabel").text("Vehicle Type");
            $("#btnVehicleType").css('font-weight', 'bold');
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#btnVehicleLocation").css("font-weight", "normal");
            //Removing the list of markers from the array. 
            for (i = 0; i < gmarkers.length; i++) {
                gmarkers[i].setMap(null);
            }
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: 'images/truck.png',
                });
                gmarkers.push(marker);
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        infowindow.setContent(infoWindowContent[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close(map, marker);
                });
            }
            //cflag = 0;
            vflag = 1;
            //hflag = 0;
            if (hflag == 1) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                if (cflag == 1) {   //Show all crews                       
                    $("#btnShowCrew").css('font-weight', 'bold');
                    for (i = 0; i < crew.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                            map: map,
                            icon: 'images/crew.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(crew[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });

                    }
                }
            }
            else if (cflag == 1) {   //Show all crews                       
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
            }
        });

        var hflag = 0;
        //function to call ShowHydrants() .
        $("#btnHydrant").click(function () {
            $("#spnLabel").text("Hydrants");
           // hflag = hflag + 1;
            ShowHydrants();
        });

       
        //function to call ShowAllVehicle() .
        $("#btnDropMarker").click(function () {
            $("#spnLabel").text("Drop Marker");
            //function that will add markers on button click
            ///----------------------------------For lister marker based drawing tool---------------------
                var markers = [];
                var latLong = [];
                var centroidPoints = [];
                var myLatlng;
                var path = new google.maps.MVCArray;
                myLatlng =  new google.maps.LatLng(38.866574, -77.0094);
                var myOptions = {
                    zoom: 12,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                    }
                }
                map.setOptions({ draggableCursor: 'crosshair' });
                var evtListnr = google.maps.event.addListener(map, 'click', function (event) {
                    path.insertAt(path.length, event.latLng);
                    var marker = new google.maps.Marker({
                        position: event.latLng,
                        icon: 'images/target.png',
                        map: map,
                        draggable: false
                    });
                    markers.push(marker);
                    latLong.push(marker.getPosition())
                   // alert(latLong.length);
                    marker.setTitle("Water pipe has broken");
                    if (path.length == 1) {
                        google.maps.event.removeListener(evtListnr);
                        //getCenterd();
                        map.setOptions({ draggableCursor: 'undefined' });
                    }                   
                    google.maps.event.addListener(marker, 'click', function (event) {
                        
                    });

                });
        });

        
        //function to all vehicles.
        function ShowAllVehicle() {
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "normal");
            if (vflag == 0) {
                $("#btnShowVehicles").css('font-weight', 'bold');
                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        icon: 'images/truck.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoWindowContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                vflag = 1;
            }
            else {
                $("#btnShowVehicles").css('font-weight', 'normal');               
                vflag = 0;
                //Code to show hydrants
                if (hflag == 1) {                    
                    $("#btnHydrant").css('font-weight', 'bold');
                    for (i = 0; i < gmarkers.length; i++) {
                        gmarkers[i].setMap(null);
                    }
                    for (i = 0; i < hydrant.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                            map: map,
                            icon: 'images/hydrant.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(infoHydrantContent[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });
                    }
                    if (cflag == 1) {   //Show all crews                       
                        $("#btnShowCrew").css('font-weight', 'bold');
                        for (i = 0; i < crew.length; i++) {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                                map: map,
                                icon: 'images/crew.png',
                            });
                            gmarkers.push(marker);
                            google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                                return function () {
                                    infowindow.setContent(crew[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                            google.maps.event.addListener(marker, 'mouseout', function () {
                                infowindow.close(map, marker);
                            });

                        }
                    }
                }                
                else {                    
                    $("#btnHydrant").css('font-weight', 'normal');
                    $("#btnShowCrew").css('font-weight', 'normal');
                    if (cflag == 1) {   //Show all crews  
                        //Removing the list of markers from the array.
                        for (i = 0; i < gmarkers.length; i++) {
                            gmarkers[i].setMap(null);
                        }
                        $("#btnShowCrew").css('font-weight', 'bold');
                        for (i = 0; i < crew.length; i++) {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                                map: map,
                                icon: 'images/crew.png',
                            });
                            gmarkers.push(marker);
                            google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                                return function () {
                                    infowindow.setContent(crew[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                            google.maps.event.addListener(marker, 'mouseout', function () {
                                infowindow.close(map, marker);
                            });

                        }
                    }
                    else {
                        //Removing the list of markers from the array.
                        for (i = 0; i < gmarkers.length; i++) {
                            gmarkers[i].setMap(null);
                        }
                    }
                }                
            }
        }

        var cflag = 0;
        // Show Crew
        $("#btnShowCrew").click(function () {
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "normal");
            $("#spnLabel").text("Crews");
            if (cflag == 0) {
                $("#btnShowCrew").css('font-weight', 'bold');
                for (i = 0; i < crew.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                        map: map,
                        icon: 'images/crew.png',
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(crew[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });

                }
                cflag = 1;
            }           
            else {
                $("#btnShowCrew").css('font-weight', 'normal');
                cflag = 0;
                //Code for show all vehicle
                if (vflag == 1) {
                    for (i = 0; i < gmarkers.length; i++) {
                        gmarkers[i].setMap(null);
                    }
                    $("#btnShowVehicles").css('font-weight', 'bold');
                    for (i = 0; i < locations.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            map: map,
                            icon: 'images/truck.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(infoWindowContent[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });
                    }
                    if (hflag == 1) {  //Code to show hydrants
                        $("#btnHydrant").css('font-weight', 'bold');                        
                        for (i = 0; i < hydrant.length; i++) {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                                map: map,
                                icon: 'images/hydrant.png',
                            });
                            gmarkers.push(marker);
                            google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                                return function () {
                                    infowindow.setContent(infoHydrantContent[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                            google.maps.event.addListener(marker, 'mouseout', function () {
                                infowindow.close(map, marker);
                            });
                        }
                    }
                }                
                else {
                    $("#btnShowVehicles").css('font-weight', 'normal');
                    $("#btnHydrant").css('font-weight', 'normal');
                    if (hflag == 1) {  //Code to show hydrants
                        //Removing the list of markers from the array. 
                        for (i = 0; i < gmarkers.length; i++) {
                            gmarkers[i].setMap(null);
                        }
                        $("#btnHydrant").css('font-weight', 'bold');
                        for (i = 0; i < hydrant.length; i++) {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                                map: map,
                                icon: 'images/hydrant.png',
                            });
                            gmarkers.push(marker);
                            google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                                return function () {
                                    infowindow.setContent(infoHydrantContent[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                            google.maps.event.addListener(marker, 'mouseout', function () {
                                infowindow.close(map, marker);
                            });
                        }
                    }
                    else {
                        //Removing the list of markers from the array. 
                        for (i = 0; i < gmarkers.length; i++) {
                            gmarkers[i].setMap(null);
                        }
                    }
                }               
            }
        });


        //function to show hydrants
        function ShowHydrants() {
            $("#aInShop").css("font-weight", "normal");
            $("#aInActive").css("font-weight", "normal");
            $("#aActive").css("font-weight", "normal");
            $("#aVan").css("font-weight", "normal");
            $("#a7Passenger").css("font-weight", "normal");
            $("#a15Passenger").css("font-weight", "normal");
            $("#aTruck").css("font-weight", "normal");
            $("#aPickUp").css("font-weight", "normal");
            $("#aUtilityLight").css("font-weight", "normal");
            $("#btnVehicleLocation").css("font-weight", "normal");
            $("#btnVehicleType").css("font-weight", "normal");
            if (hflag == 0) {
                $("#btnHydrant").css('font-weight', 'bold');
                for (i = 0; i < hydrant.length; i++) {                 

                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(hydrant[i][1], hydrant[i][2]),
                        map: map,
                        icon: 'images/hydrant.png',                        
                    });
                    gmarkers.push(marker);
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            infowindow.setContent(infoHydrantContent[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow.close(map, marker);
                    });
                }
                hflag = 1;
            }
            else {
                $("#btnHydrant").css('font-weight', 'normal');               
                hflag = 0;           
                //Code for show all vehicle
                if (vflag == 1) {
                    for (i = 0; i < gmarkers.length; i++) {
                        gmarkers[i].setMap(null);
                    }
                    $("#btnShowVehicles").css('font-weight', 'bold');
                    for (i = 0; i < locations.length; i++) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            map: map,
                            icon: 'images/truck.png',
                        });
                        gmarkers.push(marker);
                        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                            return function () {
                                infowindow.setContent(infoWindowContent[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                        google.maps.event.addListener(marker, 'mouseout', function () {
                            infowindow.close(map, marker);
                        });
                    }
                    if (cflag == 1) {   //Show all crews                       
                        $("#btnShowCrew").css('font-weight', 'bold');
                        for (i = 0; i < crew.length; i++) {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                                map: map,
                                icon: 'images/crew.png',
                            });
                            gmarkers.push(marker);
                            google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                                return function () {
                                    infowindow.setContent(crew[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                            google.maps.event.addListener(marker, 'mouseout', function () {
                                infowindow.close(map, marker);
                            });
                        }
                    }
                }                
                else {
                    $("#btnShowVehicles").css('font-weight', 'normal');
                    $("#btnShowCrew").css('font-weight', 'normal');
                    if (cflag == 1) {   //Show all crews      
                        //Removing the list of markers from the array. 
                        for (i = 0; i < gmarkers.length; i++) {
                            gmarkers[i].setMap(null);
                        }
                        $("#btnShowCrew").css('font-weight', 'bold');
                        for (i = 0; i < crew.length; i++) {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(crew[i][1], crew[i][2]),
                                map: map,
                                icon: 'images/crew.png',
                            });
                            gmarkers.push(marker);
                            google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                                return function () {
                                    infowindow.setContent(crew[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                            google.maps.event.addListener(marker, 'mouseout', function () {
                                infowindow.close(map, marker);
                            });
                        }
                    }
                    else {
                        //Removing the list of markers from the array. 
                        for (i = 0; i < gmarkers.length; i++) {
                            gmarkers[i].setMap(null);
                        }
                    }
                }      
            }
        }        
    </script>
</asp:Content>

